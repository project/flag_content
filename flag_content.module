<?php

// Copyright 2006-2007 Khalid Baheyeldin http://2bits.com

define('FLAG_CONTENT_NODE_TYPE',    'flag_content_node_type_');
define('FLAG_CONTENT_PERM_ADD',     'flag content');
define('FLAG_CONTENT_PERM_ADMIN',   'administer flag content');
define('FLAG_CONTENT_PERM_MANAGE',  'manage flagged list');
define('FLAG_CONTENT_EMAIL',        'flag_content_email');
define('FLAG_CONTENT_USER',         'flag_content_user');
define('FLAG_CONTENT_COMMENT',      'flag_content_comment');
define('FLAG_CONTENT_TYPE_NODE',    'node');
define('FLAG_CONTENT_TYPE_USER',    'user');
define('FLAG_CONTENT_TYPE_COMMENT', 'comment');

function flag_content_help($section) {
  switch ($section) {
    case 'admin/help#flag_content':
      return t('Allow users to flag nodes, users or comments for the administrator to review.');
  }
}

function flag_content_perm() {
  return array(FLAG_CONTENT_PERM_ADD, FLAG_CONTENT_PERM_MANAGE, FLAG_CONTENT_PERM_ADMIN);
}

function flag_content_menu($may_cache) {
  $items = array();
  if ($may_cache) {
    $items[] = array(
      'path'               => 'admin/settings/flag_content',
      'title'              => t('Flag content'),
      'description'        => t('Settings for the flag content module.'),
      'callback'           => 'drupal_get_form',
      'callback arguments' => array('flag_content_admin_settings'),
      'access'             => user_access(FLAG_CONTENT_PERM_ADMIN),
    );

    $items[] = array(
      'path'               => 'flag_content/add',
      'callback'           => 'drupal_get_form',
      'callback arguments' => array('flag_content_add'),
      'type'               => MENU_CALLBACK,
      'access'             => user_access(FLAG_CONTENT_PERM_ADD),
    );

    $items[] = array(
      'path'               => 'flag_content/unflag',
      'callback'           => 'drupal_get_form',
      'callback arguments' => array('flag_content_unflag'),
      'type'               => MENU_CALLBACK,
      'access'             => user_access(FLAG_CONTENT_PERM_MANAGE),
    );

    $items[] = array(
      'path'               => 'admin/content/flag_content',
      'callback'           => 'flag_content_view',
      'title'              => t('Flagged items'),
      'description'        => t('View, edit, delete flagged nodes, comments, or users.'),
      'access'             => user_access(FLAG_CONTENT_PERM_MANAGE),
    );
  }
  return $items;
}

function flag_content_admin_settings() {
  $set = 'types';
  $form[$set] = array(
    '#type' => 'fieldset',
    '#title' => t('Enable flagging for these content types'),
  );

  foreach(node_get_types() as $node_type) {
    $type = $node_type->type;
    $name = $node_type->name;
    $form[$set][FLAG_CONTENT_NODE_TYPE . $type] = array(
      '#type' => 'checkbox',
      '#title' => $name,
      '#return_value' => 1,
      '#default_value' => variable_get(FLAG_CONTENT_NODE_TYPE . $type, 0),
    );
  }
  $set = 'other';
  $form[$set] = array(
    '#type' => 'fieldset',
    '#title' => t('Enable flagging for users and comments'),
  );
  $form[$set][FLAG_CONTENT_USER] = array(
    '#type' => 'checkbox',
    '#title' => t('Flag users'),
    '#return_value' => 1,
    '#default_value' => variable_get(FLAG_CONTENT_USER, 0),
  );

  $form[$set][FLAG_CONTENT_COMMENT] = array(
    '#type' => 'checkbox',
    '#title' => t('Flag comments'),
    '#return_value' => 1,
    '#default_value' => variable_get(FLAG_CONTENT_COMMENT, 0),
  );

  $form['email'][FLAG_CONTENT_EMAIL] = array(
    '#type' => 'textfield',
    '#title' => t('Email address'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => true,
    '#default_value' => variable_get(FLAG_CONTENT_EMAIL, variable_get('site_mail', ini_get('sendmail_from'))),
    '#description' => t('Email address to send alerts on flagged items to.'),
  );

  return system_settings_form($form);
}

/**
 *  Implementation of hook_user().
 */
function flag_content_user($op, &$edit, &$account, $category = NULL) {
  global $user;

  switch($op) {
    case 'view':
      if ($user->uid == $account->uid) {
        // User is viewing their own user page, don't show them anything
        return array();
      }

      $items = array();
      $links = flag_content_link(FLAG_CONTENT_TYPE_USER, $account);
      foreach($links as $key => $value) {
        $items[] = array(
          'class' => $key,
          'value' => l($value['title'], $value['href'], $value['attributes']),
        );
      }
     
      if (is_array($items)) {
        return array(t('Flag user') => $items);       
      }
      break;

    case 'delete':
      db_query("DELETE FROM {flag_content} WHERE eid = %d AND type = '%s'", $account->uid, FLAG_CONTENT_TYPE_USER);
      break;
   }
}

/**
 *  Implementation of hook_comment().
 */
function flag_content_comment($a1, $op) {
  switch ($op) {
    case 'delete':
      db_query("DELETE FROM {flag_content} WHERE eid = %d AND type = '%s'", $a1->cid, FLAG_CONTENT_TYPE_COMMENT);
      break;
  }
}

/**
 *  Implementation of hook_link().
 *  $entry is actually $node but since this is supporting both users and nodes, we call it $entry (eventually support comments?)
 */

function flag_content_link($type, $entry = null, $teaser = false) {
  global $user;
  $links = array();
  
  // Do not allow users to flag themselves.
  if ($type == FLAG_CONTENT_TYPE_USER && $entry->uid == $user->uid) {
    return $links;
  }
  
  if (user_access(FLAG_CONTENT_PERM_ADD)) {
    switch ($type) {
      case FLAG_CONTENT_TYPE_NODE:
        if (variable_get(FLAG_CONTENT_NODE_TYPE . $entry->type, 0)) {
          $eid = $entry->nid;
        }
        break;
      case FLAG_CONTENT_TYPE_USER:
        if (variable_get(FLAG_CONTENT_USER, 0)) {
          $eid = $entry->uid;
        }
        break;
      case FLAG_CONTENT_TYPE_COMMENT:
        if (variable_get(FLAG_CONTENT_COMMENT, 0)) {
          $eid = $entry->cid;
        }
        break;
    }
    
    if (is_numeric($eid)) {
      $options = array('class' => 'flag_content');
  
      if (!_flag_content_check($eid, $type)) {
        // Not already flagged, flag it for admin
        $links['flag_content_add'] = array(
          'title' => t('flag this'),
          'href' => "flag_content/add/$eid/$type",
          'attributes' => array(
            'class' => 'flag_content',
            'title' => t('Notify the administrators this posting is problematic')
            )
          );
      }
      else {
        // If has admin privileges, show an unflag link
        if (user_access(FLAG_CONTENT_PERM_MANAGE)) {
          $links['flag_content_unflag'] = array(
            'title' => t('unflag'),
            'href' => "flag_content/unflag/$eid/$type",
            'attributes' => array(
              'class' => 'flag_content',
              'title' => t('Remove flagged marking')
              )
            );
        }
        else {
          // Otherwise just show it as flagged
          $links['flag_content_remove_flag'] = array(
            'title' => t('flagged'),
          );
        }
      }
    }
  }
  
  return $links;
}

function flag_content_add($eid = 0, $type = FLAG_CONTENT_TYPE_NODE) {
  switch ($type) {
    case FLAG_CONTENT_TYPE_NODE:
      $node = node_load($eid);
      $title = $node->title;
      $type_label = $node->type;
      $path = $type . '/' . $eid;
      break;
    case FLAG_CONTENT_TYPE_USER:
      $account = user_load(array('uid' => $eid));
      $title = $account->name;
      $type_label = $type;
      $path = $type . '/' . $eid;
      break;
    case FLAG_CONTENT_TYPE_COMMENT:
      $comment = db_fetch_object(db_query('SELECT * FROM {comments} WHERE cid = %d', $eid));
      $title = $comment->subject;
      $type_label = $type;
      $path = "node/$comment->nid";
      break;
  }
  
  $form['eid'] = array(
    '#type' => 'hidden',
    '#value' => $eid,
  );

  $form['type'] = array(
    '#type' => 'hidden',
    '#value' => $type,
  );

  return confirm_form(
    $form,
    t('Are you sure you want to flag the @type @title', array('@type' => $type_label, '@title' => $title)),
    $_GET['destination'] ? $_GET['destination'] : $path,
    t(''),
    t('Flag'),
    t('Cancel'));
}

function flag_content_add_submit($form_id, $form_values) {
  global $user;
  $eid = $form_values['eid'];
  $type = $form_values['type'];
  if ($eid && $type) {
    // Insert the data into the table
    db_query("INSERT INTO {flag_content} (fid, eid, uid, type, timestamp) VALUES (0, %d, %d, '%s', %d)",
      $eid, $user->uid, $type, time());
    // Prepare the data
    switch ($type) {
      case FLAG_CONTENT_TYPE_NODE:
        $entry = node_load(array('nid' => $eid));
        $path = $type . '/' . $eid;
        $title = $entry->title;
        break;
      case FLAG_CONTENT_TYPE_USER:
        $entry = user_load(array('uid' => $eid));
        $path = $type . '/' . $eid;
        $title = $entry->name;
        break;
      case FLAG_CONTENT_TYPE_COMMENT:
        $entry = db_fetch_object(db_query('SELECT * FROM {comments} WHERE cid = %d', $eid));
        $path = "node/$entry->nid";
        $title = $entry->subject;
        break;
    }

    // Email the admin
    $email = theme('flag_content_mail', $entry, $type);
    drupal_mail('flag_content_admin', $email['to'], $email['subject'], $email['body'], $email['from']);

    // Log the operation:
    watchdog('flag content', t("@name flagged item @entry for review.",
      array ('@name' => theme('placeholder', $user->name ." <" . $email['from'] . ">"), '@entry' => l($title, $path))));

    // Give feedback to the user
    drupal_set_message(t('The @type was flagged for the administrator', array('@type' => $type == FLAG_CONTENT_TYPE_NODE ? t('item') : $type)));
  }

  return $_GET['destination'] ? $_GET['destination'] : $path;
}

function flag_content_unflag($eid = 0, $type = FLAG_CONTENT_TYPE_NODE) {
  switch ($type) {
    case FLAG_CONTENT_TYPE_NODE:
      $node = node_load($eid);
      $title = $node->title;
      $type_label = $node->type;
      break;
    case FLAG_CONTENT_TYPE_USER:
      $account = user_load(array('uid' => $eid));
      $title = $account->name;
      $type_label = $type;
      break;
    case FLAG_CONTENT_TYPE_COMMENT:
      $comment = db_fetch_object(db_query('SELECT * FROM {comments} WHERE cid = %d', $eid));
      $title = $comment->subject;
      $type_label = $type;
      break;
  }

  $form['eid'] = array(
    '#type' => 'hidden',
    '#value' => $eid,
  );
  $form['type'] = array(
    '#type' => 'hidden',
    '#value' => $type,
  );
  return confirm_form(
    $form,
    t('Are you sure you want to unflag the @type @title', array('@type' => $type_label, '@title' => $title)),
    $_GET['destination'] ? $_GET['destination'] : $type. '/'. $eid,
    t(''),
    t('Unflag'),
    t('Cancel'));
}

function flag_content_unflag_submit($form_id, $form_values) {
  $eid = $form_values['eid'];
  $type = $form_values['type'];
  if ($eid && $type) {
    _flag_content_unflag($eid, $type);
    drupal_set_message(t('The @type was unflagged.', array('@type' => $type == FLAG_CONTENT_TYPE_NODE ? t('item') : $type)));
  }
  return 'admin/content/flag_content';
}

function flag_content_view() {
  print theme('page', theme('flag_content_view', _flag_content_get_list()));
}

function theme_flag_content_view($list = array()) {
  $rows = array();
  $header = array(t('Item'), t('Type'), t('Flagged by'), t('Date'), t('Operations'));
  if (!empty($list)) {
    foreach($list as $entry) {
      switch ($entry->type) {
        case FLAG_CONTENT_TYPE_NODE:
          $node = node_load(array('nid' => $entry->eid));
          $title = l($node->title, 'node/'. $node->nid, array('title' => $node->title)); 
          $author = theme('username', user_load(array('uid' => $node->uid)));
          $item = t('!title - (author: !author)', array('!title' => $title, '!author' => $author));
          $edit = "node/$entry->eid/edit";
          $delete = "node/$entry->eid/delete";
          $type = $node->type;
          break;
        case FLAG_CONTENT_TYPE_USER:
          $item = theme('username', user_load(array('uid' => $entry->eid)));
          $edit = "user/$entry->eid/edit";
          $delete = "user/$entry->eid/delete";
          $type = $entry->type;
          break;
        case FLAG_CONTENT_TYPE_COMMENT:
          $comment = db_fetch_object(db_query('SELECT * FROM {comments} WHERE cid = %d', $entry->eid));
          $subject = $comment->subject ? $comment->subject : 'comment';
          $item = l($subject, url('node/' . $comment->nid, NULL, NULL, 1) . "#comment-" . $comment->cid);
          $edit = "comment/edit/$comment->cid";
          $delete = "comment/delete/$comment->cid";
          $type = $entry->type;
          break;
      }

      $timestamp = format_date($entry->timestamp, 'custom', 'Y-m-d H:i');
      $by = theme('username', user_load(array('uid' => $entry->uid)));
      $ops = l(t('edit'), $edit);
      $ops .= ' ' . l(t('unflag'), "flag_content/unflag/$entry->eid/$entry->type");
      $ops .= ' ' . l(t('delete'), $delete);
      $rows[] = array($item, $type, $by, $timestamp, $ops);
    }
  }
  else {
    $rows[] = array('data' => array(t('No flagged items.')));
  }
  return theme('table', $header, $rows);
}

function flag_content_nodeapi(&$node, $op, $teaser = NULL, $page = NULL) {
  switch ($op) {
    case 'delete':
      // Node is being deleted, delete it from the flag_content table
      _flag_content_unflag($node->nid);
      break;
  }
}

function _flag_content_unflag($eid, $type = FLAG_CONTENT_TYPE_NODE) {
  db_query("DELETE FROM {flag_content} WHERE eid = %d AND type = '%s'", $eid, $type);
}

function _flag_content_get_list() {
  $rows = array();
  $result = db_query("SELECT * FROM {flag_content} ORDER BY timestamp ASC");
  while ($row = db_fetch_object($result)) {
    $rows[$row->fid] = $row;
  }
  return $rows;
}

function _flag_content_check($eid, $type = FLAG_CONTENT_TYPE_NODE) {  
  global $user;
  return db_result(db_query("SELECT COUNT(*) FROM {flag_content} WHERE eid = %d AND type = '%s'", $eid, $type));
}

function theme_flag_content_mail($entry = NULL, $type = FLAG_CONTENT_TYPE_NODE) {
  global $user, $base_url;
  switch ($type) {
    case FLAG_CONTENT_TYPE_NODE:
      $title = $entry->title;
      $eid = $entry->nid;
      $path = "$type/$eid";
      break;
    case FLAG_CONTENT_TYPE_USER:
      $title = $entry->name;
      $eid = $entry->uid;
      $path = "$type/$eid";
      break;
    case FLAG_CONTENT_TYPE_COMMENT:
      $title = $entry->subject;
      $eid = $entry->cid;
      $path = url('node/' . $entry->nid, NULL, NULL, 0) . "#comment-" . $entry->cid;
      break;
  }

  if ($user->uid) {
    $name = $user->name;
    $from = $user->mail;
    $user_url = $base_url . base_path() . "user/$user->uid";
  }
  else {
    $name = variable_get('anonymous', 'Anonymous');
    $from = variable_get('site_mail', '');
    $user_url = t('n/a');
  }
  $site = variable_get('site_name', 'drupal site');
  $to = variable_get(FLAG_CONTENT_EMAIL, variable_get('site_mail', ini_get('sendmail_from')));
  $subject = "[Flag]: [$title] from [$site]";
  $body  = t("The following user flagged an item for your review.\n\nUser: @name (@user_url)", array(
    '@name'     => $name,
    '@user_url' => $user_url,
    ));
  $body .= t("\nItem: @title (@entry_url)", array(
    '@title'     => $title,
    '@entry_url' => $base_url . base_path() . $path,
    ));

  $body .= t("\n\nManage the flagged items list at @manage", array(
    '@manage' => $base_url . base_path() . 'admin/content/flag_content',
    ));

  $to = check_plain($to);
  $from = check_plain($from);
  $name = check_plain($name);

  return array(
    'to'      => $to,
    'from'    => $from,
    'subject' => $subject,
    'body'    => $body,
  );
}
